package com.dh.di1demo.controller;

import com.dh.di1demo.services.GreetingService;

public class GetterBassedController {
    public GreetingService greetingService;
    public String sayHello(){
        return greetingService.sayGreeting();
    }

    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }
}
