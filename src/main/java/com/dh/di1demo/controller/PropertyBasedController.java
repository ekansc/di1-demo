package com.dh.di1demo.controller;

import com.dh.di1demo.services.GreetingService;
import org.springframework.stereotype.Controller;

@Controller
public class PropertyBasedController {
    public GreetingService greetingService;
    public String sayHello(){
        return greetingService.sayGreeting();
    }
}
