package com.dh.di1demo.controller;

import com.dh.di1demo.services.GreetingService;
import com.dh.di1demo.services.GreetingServiceImpl;
import org.springframework.stereotype.Controller;

@Controller
public class ConstructorBasedController {
    private GreetingService greetingService;

    public ConstructorBasedController() {
        greetingService = new GreetingServiceImpl();
    }

    public ConstructorBasedController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String sayHello(){
        return greetingService.sayGreeting();
    }

//    public void setGreetingService(GreetingService greetingService) {
//        this.greetingService = greetingService;
//    }
}
