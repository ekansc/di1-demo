package com.dh.di1demo;

import com.dh.di1demo.controller.MyController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Di1DemoApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Di1DemoApplication.class, args);
		MyController controller = (MyController) context.getBean("myController");
		controller.hello();
	}
}
