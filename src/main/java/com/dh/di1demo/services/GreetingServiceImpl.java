package com.dh.di1demo.services;

public class GreetingServiceImpl implements GreetingService {
    public static String GREETING= "Hello GreetingServiceImpl";

    @Override
    public String sayGreeting() {
        return this.GREETING;
    }
}
