package com.dh.di1demo.services;

public interface GreetingService {
    String sayGreeting();
}
