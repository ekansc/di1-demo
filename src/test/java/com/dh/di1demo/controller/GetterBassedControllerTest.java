package com.dh.di1demo.controller;

import com.dh.di1demo.services.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GetterBassedControllerTest {
    private GetterBassedController getterBassedController;

    @Before
    public void before() throws Exception{
        getterBassedController = new GetterBassedController();
        getterBassedController.setGreetingService(new GreetingServiceImpl());
    }

    @Test
    public void sayHello() {
        String greeting = getterBassedController.sayHello();
        Assert.assertEquals(GreetingServiceImpl.GREETING,greeting);
    }
}