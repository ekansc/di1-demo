package com.dh.di1demo.controller;

import com.dh.di1demo.services.GreetingService;
import com.dh.di1demo.services.GreetingServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConstructorBasedControllerTest {
    private ConstructorBasedController constructorBasedController;



    @Before
    public void setUp() throws Exception {
        System.out.println("@Before");
        GreetingService greetingService = new GreetingServiceImpl();
        constructorBasedController = new ConstructorBasedController(greetingService);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("@After");
    }

    @Test
    public void sayHello() {
        System.out.println("@Test");
        String greeting = constructorBasedController.sayHello();
        Assert.assertEquals(GreetingServiceImpl.GREETING,greeting);
    }
}