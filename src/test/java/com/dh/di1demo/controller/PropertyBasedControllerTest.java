package com.dh.di1demo.controller;

import com.dh.di1demo.services.GreetingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PropertyBasedControllerTest {

    private PropertyBasedController propertyBasedController;

    @Before
    public void before(){
        propertyBasedController = new PropertyBasedController();
        propertyBasedController.greetingService=new GreetingServiceImpl();
    }


    @Test
    public void sayHello() {
        String greeting = propertyBasedController.sayHello();
        Assert.assertEquals(GreetingServiceImpl.GREETING,greeting);
    }
}